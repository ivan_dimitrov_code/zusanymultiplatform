package com.puzzlegramma.myapplication.api

import com.puzzlegramma.myapplication.ProductResponse

interface Api {
    suspend fun getProducts(): List<ProductResponse>
}