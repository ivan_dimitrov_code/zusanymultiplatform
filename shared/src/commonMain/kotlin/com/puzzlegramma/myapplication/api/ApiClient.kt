package com.puzzlegramma.myapplication.api

import com.puzzlegramma.myapplication.ProductResponse
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.*
import kotlinx.serialization.json.Json

class ApiClient : Api {

    private val httpClient = HttpClient {
        install(JsonFeature) {
            val json = Json { ignoreUnknownKeys = true }
            serializer = KotlinxSerializer(json)
        }
    }

    override suspend fun getProducts(): List<ProductResponse> {
        return httpClient.get("https://zusany.com/wp-json/wc/v2/products?consumer_key=ck_66ec9a155c3dfe3e430bd8972447d12dc7dee655&consumer_secret=cs_5f17ae5bea86221c175416ae227db653a537b6da")
    }
}