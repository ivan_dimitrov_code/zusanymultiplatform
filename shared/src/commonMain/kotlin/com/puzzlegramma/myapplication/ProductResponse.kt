package com.puzzlegramma.myapplication

import kotlinx.serialization.*

@Serializable
class ProductResponse {
    var id: Int = 0
    var name: String? = null
    var price: String? = null
    var images: List<ProductImageData>? = null
}

@Serializable
class ProductImageData {
    var src: String? = null
}