package com.puzzlegramma.myapplication.android

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.puzzlegramma.myapplication.ProductResponse
import com.puzzlegramma.myapplication.android.databinding.FragmentItemBinding
import com.squareup.picasso.Picasso


class MyItemRecyclerViewAdapter(
    private val values: MutableList<ProductResponse>
) : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun injectList(values: List<ProductResponse>) {
        this.values.clear()
        this.values.addAll(values)
        notifyItemChanged(0)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        Picasso.get().load(item.images?.get(0)?.src.toString()).into(holder.image)
        holder.contentView.text = item.name
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val image: ImageView = binding.imageView
        val contentView: TextView = binding.content

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }

}