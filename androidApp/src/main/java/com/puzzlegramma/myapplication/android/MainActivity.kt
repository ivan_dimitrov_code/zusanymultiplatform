package com.puzzlegramma.myapplication.android

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.puzzlegramma.myapplication.android.databinding.ActivityMainBinding
import com.puzzlegramma.myapplication.api.ApiClient
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private val api = ApiClient()
    private val mainScope = MainScope()
    var binding: ActivityMainBinding? = null
    private val adapter: MyItemRecyclerViewAdapter = MyItemRecyclerViewAdapter(mutableListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        binding?.list?.layoutManager = LinearLayoutManager(this@MainActivity)
        binding?.list?.adapter = adapter

        mainScope.launch {
            kotlin.runCatching {
                api.getProducts()
            }.onSuccess {
                Toast.makeText(this@MainActivity, "data", Toast.LENGTH_SHORT).show()
                adapter.injectList(it)
            }.onFailure {

            }
        }
    }
}
