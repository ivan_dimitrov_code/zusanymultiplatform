import SwiftUI
import shared

struct ContentView: View {
    let greet = ApiClient()
    
    
    @State var products:  [ProductResponse] = []
    
    func load() {
        greet.getProducts {result, error in
            products = result ?? []
        }
    }
    
    var body: some View {
        List(products, id: \.self) { product in
            HStack(){
                if #available(iOS 15.0, *) {
                    AsyncImage(url: URL(string: product.images?[0].src ?? "")) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 50, height: 50)
                } else {
                    // Fallback on earlier versions
                }
                Text(product.name ?? "Not found")
            }
        }.onAppear(){
            load()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
