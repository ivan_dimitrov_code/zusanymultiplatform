//
//  ProductRow.swift
//  iosApp
//
//  Created by Ivan on 9.02.22.
//  Copyright © 2022 orgName. All rights reserved.
//
import SwiftUI
import shared

struct ProductRow: View {
    var product: ProductResponse

    var body: some View {
        HStack {
//            product.image
//                .resizable()
//                .frame(width: 50, height: 50)
            Text(product.name ?? "NO_NAME")

            Spacer()
        }
    }
}
